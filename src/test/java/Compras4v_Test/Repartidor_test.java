package Compras4v_Test;

import static org.junit.jupiter.api.Assertions.*;

import org.cuatrovieontos.compras4v.Repartidor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class Repartidor_test {
	Repartidor repartidor;
	@BeforeEach
	void setUp() throws Exception {
		repartidor = new Repartidor("Mikel","Albarez",19,400d,"the bronx");
	}

	@Test
	void test() {
		
		assertEquals( repartidor.toString(),"nombre= Mikel apellido= Albarez edad= 19 salario= 400.0 zona= the bronx ");
		
	}

}
