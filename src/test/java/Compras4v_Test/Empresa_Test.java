package Compras4v_Test;

import static org.junit.jupiter.api.Assertions.*;

import org.cuatrovieontos.compras4v.Comercial;
import org.cuatrovieontos.compras4v.Empresa;
import org.cuatrovieontos.compras4v.Repartidor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class Empresa_Test {
	Empresa empresa;
	Comercial comercial;
	Repartidor repartidor;
	@BeforeEach
	void setUp() throws Exception {
		empresa = new Empresa("fancy.Tor","19216811");
		repartidor = new Repartidor("Mikel","Albarez",19,400d,"the bronx");
		comercial = new Comercial("Liher","Gallego",25,1300d,150d);
		empresa.addEmpleado(repartidor);
		empresa.addEmpleado(comercial);
	}

	@Test
	void test() {
		assertEquals(empresa.totalSalario(), 1700d);
	}

}
