/**
 * 
 */
package Compras4v_Test;

import static org.junit.jupiter.api.Assertions.*;

import org.cuatrovieontos.compras4v.Comercial;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * @author liher
 *
 */
class Comercial_test {

	/**
	 * @throws java.lang.Exception
	 */
	Comercial comercial;
	@BeforeEach
	void setUp() throws Exception {
		comercial = new Comercial("Liher","Gallego",25,1300d,150d);
		
	}

	@Test
	void test() {
		assertEquals(comercial.getNombre(), "Liher");
		assertEquals(comercial.getApellido(), "Gallego");
		assertEquals(comercial.getEdad(), 25);
		assertEquals(comercial.getSalario(),1300d);
		assertEquals(comercial.getComision(),150d);
		assertTrue(comercial.plus(100d));
		
	}

}
