package org.cuatrovieontos.compras4v;
/**
 * 
 * @author liher
 *
 */
public abstract class Empleado {
	
	protected String nombre;
	protected String apellido;
	protected int edad;
	protected double salario;
	
	public Empleado(){
		this.nombre = "";
		this.apellido = "";
		this.edad = 0;
		this.salario = 0.0;
	}
	
	public Empleado(String nombre, String apellido, int edad, double salario) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.edad = edad;
		this.salario = salario;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}
	
	public abstract boolean plus(double valor);
	
	private boolean compruevaNombre (String valor) {
		if (valor == this.nombre) {
			return true;
		}else {
			return false;
		}	
	}

	@Override
	public String toString() {
		String empleado = "";
		empleado += "nombre= " + this.nombre + " ";
		empleado += "apellido= " + this.apellido + " ";
		empleado += "edad= " + this.edad + " ";
		empleado += "salario= " + this.salario + " ";
		return empleado;
	}
	
	
}


