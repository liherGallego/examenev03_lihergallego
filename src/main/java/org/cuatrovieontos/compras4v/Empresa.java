package org.cuatrovieontos.compras4v;

import java.util.ArrayList;

/**
 * Esta clase contiene el nombre de la empresa y su nif
 * Tambien contiene una lista de empleados
 * @author liher
 *
 */
public class Empresa {

	private String nombre;
	private String nif;
	private ArrayList<Empleado> empleados; 
	
	public Empresa(String nombre, String nif) {
		empleados = new ArrayList<Empleado>();
		this.nombre = nombre;
		this.nif = nif;
	}
	
	public void addEmpleado(Empleado emp) {
		empleados.add(emp);
	}
	
	public double totalSalario() {
		double salarioTotal = 0;
		for (Empleado empl:empleados) {
			salarioTotal += empl.salario;
		}
		return salarioTotal;
	}
	
	public void actualizarExtra(double extra) {
		for (Empleado empl:empleados) {
			if (empl.plus(extra)){
				System.out.println(empl.nombre + " Extra actualizado");
			}
		}
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public ArrayList<Empleado> getEmpleados() {
		return empleados;
	}

	public void setEmpleados(ArrayList<Empleado> empleados) {
		this.empleados = empleados;
	}

	@Override
	public String toString() {
		String empresa ="";
		empresa += "nombre= " + this.nombre;
		empresa += "nif= " + this.nif;
		empresa += "Empledos:" + "/n";
		for (Empleado empl:empleados) {
			empresa += empl.toString() + "/n";
		}
		return empresa;
	}
	
	
	
}
