package org.cuatrovieontos.compras4v;

/**
 * Esta clase hereda de la clase empleado.
 * Implementa la interfad IRepatir
 * Tiene un atributo propio llamado Zona
 * @author liher
 *
 */
public class Repartidor extends Empleado implements IRepartir {

	private String zona; 
	public Repartidor(String nombre, String apellido, int edad,double salario, String zona) {
		super(nombre,apellido,edad,salario);
		this.zona=zona;
	}

	public void pedalear() {
		System.out.println("txirrin-txarran");	
	}
	/**
	 * @author liher
	 * Este metodo recive un valor doueble
	 * Este valor recivido rera el incremento que se le aplique al salario del comercial siempre y cuando su sueldo sea menor a 500
	 * si se a aplicado devuelve true si no false
	 * 
	 */
	@Override
	public boolean plus(double valor) {
		if (this.salario < 500) {
			this.salario += valor;
			return true;
		}
		return false;
	}

	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}
	
	@Override
	public String toString() {
		String repartidor="";
		repartidor += super.toString();
		repartidor += "zona= " + this.zona + " ";
		return repartidor;
	}
}