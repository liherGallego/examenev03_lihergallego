package org.cuatrovieontos.compras4v;

public class Comercial extends Empleado implements IVender {
	
	/**
	 * Esta clase hereda de la clase abstracta empleado
	 * Implementa el metodo IVender
	 * Tiene un atributo llamado Comision
	 * @author liher
	 */
	private double comision;
	
	public Comercial(String nombre, String apellido, int edad, double salario, double comision) {
		super(nombre,apellido,edad,salario);
		this.comision = comision;
	}
	
	public void convencer() {
		System.out.println("Bueno,bonito,barato");

	}

	/**
	 * @author liher
	 * Este metodo recive un valor doueble
	 * Este valor recivido rera el incremento que se le aplique al salario del comercial siempre y cuando su sueldo sea menor a 2000
	 * si se a aplicado devuelve true si no false
	 * 
	 */
	@Override
	public boolean plus(double valor) {
		if (this.salario < 2000) {
			this.salario += valor;
			return true;
		}
		return false;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	@Override
	public String toString() {
		String comercial="";
		comercial += super.toString();
		comercial += "comision=" + this.comision + " ";
		return comercial;
	}
	
	
	

}
